#include <SPI.h>

// Motor
const int enA = A9;
const int in1 = A8;
const int in2 = A7;



void setup() {
  Serial.begin(9600);

  // Pin Assignment
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
}

void forwFunc(int impPwr) {
  int pwr = map(impPwr, 0, 100, 0, 76.5);
  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  analogWrite(enA, pwr);
}

void backFunc(int impPwr) {
  int pwr = map(impPwr, 0, 100, 0, 76.5);
  digitalWrite(in1, 0);
  digitalWrite(in2, 1);
  analogWrite(enA, pwr);  
}

void stopFunc() {
  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  analogWrite(enA, 0);
}


void loop() {
  delay(1000);
  stopFunc();
  for (int i = 5; i > 0; i--) {
  Serial.println(i);
  delay(500);
  if (i == 1) {
    Serial.println("Start");
    forwFunc(100);
  }
  }
  delay(1000);
  stopFunc();

  for (int i = 5; i > 0; i--) {
  Serial.println(i);
  delay(500);
  if (i == 1) {
    Serial.println("Start");
    backFunc(100);
  }
  }
  

}
