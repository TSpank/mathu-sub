#include <RH_ASK.h>
#include <SPI.h> 

// Set up signal
RH_ASK driver;
struct Signal{
  short throttle;
  short rudder;
  short pump;
};

typedef struct Signal Data;
Data data;

void setup()
{
    // Initialize ASK Object
    driver.init();
    // Setup Serial Monitor
    Serial.begin(9600);
}

void loop()
{
    // Set buffer to size of expected message
    uint8_t buf[sizeof(data)];
    uint8_t buflen = sizeof(buf);
    // Check if received packet is correct size
    if (driver.recv(buf, &buflen))
    {
      memcpy(&data, &buf, buflen);
      
      // Message received with valid checksum
      Serial.print("Throttle: ");
      Serial.println(data.throttle);   
      Serial.print("Rudder: ");
      Serial.println(data.rudder);  
      Serial.print("Pump: ");
      Serial.println(data.pump);
      Serial.println("-----------------");
      
            
    }
}
