#include <RH_ASK.h>
#include <SPI.h>
#include <ServoTimer2.h>

//pin assignment
const int V = 2;
const int MB = 3;
const int MF = 5;
const int S = 6;
const int P = 9;

//Making servo channel
ServoTimer2 servo;

RH_ASK driver;

/////////////////////////create the data object
struct Signal {
  short throttleF;
  short throttleB;
  int rudder;
  short pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

//////////////////////////Set all the data to base values
void ResetData(){
  data.throttleF = 0;
  data.throttleB = 0;
  data.rudder = 1500;
  data.pump = 0;
  data.valve = false;
}

void setup() {
  Serial.begin(9600);
  servo.attach(S);
  pinMode(MB,OUTPUT);
  pinMode(MF,OUTPUT);

  ResetData();
  if(!driver.init()){
    Serial.println("Initialisation Failed");
  }
}

//////////////////////////////motor control functions
void motorStop(){
  //Serial.println("stop");
  //Serial.println(data.throttleF);
  analogWrite(MB, 0);
  analogWrite(MF, 0);
}

void motorForw(short throttle){
  //Serial.println("forward");
  //Serial.println(throttle);
  analogWrite(MB, 0);
  analogWrite(MF, throttle);
}

void motorBack(short throttle){
  //Serial.println("back");
  //Serial.println(throttle);
  analogWrite(MB, throttle);
  analogWrite(MF, 0);
}

void turn(int pwm){
  servo.write(pwm);
}

void pump(short val_p, bool val_v){
  digitalWrite(V, val_v);
  analogWrite(P, val_p);
  analogWrite(8, 0);
}

//////////////////////loop function to recieve and handle data
void loop() {
  // put your main code here, to run repeatedly:
  uint8_t buf[sizeof(data)];  
  uint8_t buflen = sizeof(data);

  if (driver.recv(buf, &buflen)) {
    memcpy(&data, &buf, buflen);
    
    Serial.println(data.throttleF);
    Serial.println(data.rudder); 
    Serial.println(data.pump);
    Serial.println(data.valve);

    //control the throttle through
    if (data.throttleF >5){
      motorForw(data.throttleF);
      }
    else if (data.throttleB>5){
      motorBack(data.throttleB);
      }
    else{
      motorStop();
      }

    //control the valve
    if (data.valve){
      digitalWrite(V, HIGH);
      /*Serial.println("valve on");
      Serial.println(data.valve);*/
    }
    else {
      digitalWrite(V, LOW);
      /*Serial.println("valve off");
      Serial.println(data.valve);*/
    }
    
    //control the servo
    turn(data.rudder);

    //control pump
    pump(data.pump, data.valve);
  }
}
