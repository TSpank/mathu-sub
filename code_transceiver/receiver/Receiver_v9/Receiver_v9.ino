#include <RH_ASK.h>
#include <SPI.h> 
#include <ServoTimer2.h>

//pin assignment
const int V = 2;
const int P = 4;
const int S = 6;
const int enA = 5;
const int in1 = 3;
const int in2 = 12;

// Set up signal
RH_ASK driver;
struct Signal{
  short throttle;
  short rudder;
  short pump;
};

typedef struct Signal Data;
Data data;

// Making Servo Channel
ServoTimer2 servo;

void setup() {
  // Initialize ASK Object
  driver.init();
  // Setup Serial Monitor
  Serial.begin(9600);

  // Pin Setup
  servo.attach(S);
  pinMode(V, OUTPUT);
  pinMode(P, OUTPUT);
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  
}

/* Functions */
// Servo
void servoFunc(int deg) {
  int servomap = map(deg, 0, 1024, 775, 2225);
  
  if (servomap>1750){
    servo.write(servomap);
  }
  else if (servomap<1250){
    servo.write(servomap); 
  }
  else{
   servo.write(1500); 
  }  
}

// Motor
void forwFunc(int impPwr) {
  int pwr = map(impPwr, 0, 1024, 0, 89.25);
  digitalWrite(in1, 0);
  digitalWrite(in2, 1);
  analogWrite(enA, pwr);
}

void backFunc(int impPwr) {
  int pwr = map(impPwr, 0, 1024, 0, 89.25);
  int pwrN = 89.25 - pwr;
  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  analogWrite(enA, pwrN);  
}

void stopFunc() {
  digitalWrite(in1, 0);
  digitalWrite(in2, 1);
  analogWrite(enA, 0);
}

void motorFunc(int inpPwr) {
  if (inpPwr > 560) {
    Serial.println("Forward");
    forwFunc(inpPwr);
  }

  else if (inpPwr < 400) {
    Serial.println("Backwards");
    backFunc(inpPwr);
  }
  else {
    Serial.println("Stop");
    stopFunc();
  }
}

// Vavle
void valveFunc(bool openClose) {
  digitalWrite(V, openClose);  
}

// Pump
void pumpFunc(bool onOff) {
  if (onOff == true) {
    digitalWrite(P, true);
  }
  else {
    digitalWrite(P, false);
  }  
}
void pumpFuncMain (int pwr) {
  if (pwr > 700) {
    pumpFunc(true);
    valveFunc(false);
  }
  else if (pwr < 700 && pwr > 400) {
    pumpFunc(false);
    valveFunc(false);
  }
  else {
    pumpFunc(false);
    valveFunc(true);
  }
}


void loop()
{
    // Set buffer to size of expected message
    uint8_t buf[sizeof(data)];
    uint8_t buflen = sizeof(buf);
    // Check if received packet is correct size
    if (driver.recv(buf, &buflen))
    {
      memcpy(&data, &buf, buflen); 
      Serial.print("Throttle: ");
      Serial.print(data.throttle);
      Serial.print("  Rudder: ");
      Serial.print(data.rudder);
      Serial.print("  Pump: ");
      Serial.println(data.pump);
      servoFunc(data.rudder);
      pumpFuncMain(data.pump);
      motorFunc(data.throttle);
    }    
}
