#include <SPI.h>
#include <ServoTimer2.h>

#define FORW true
#define BACK false

//pin assignment
const int V = 2;
const int P = 4;
const int S = 6;
const int enA = 9;
const int in1 = 8;
const int in2 = 7;
const int VRx = A6;
const int VRy = A7;
const int SW = A5;
const int motorOnOff = 7;

// Motor Speed Control Variable
int MS;

// Making Servo channel
ServoTimer2 servo;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  //Pin Setup
  servo.attach(S);
  pinMode(V, OUTPUT);
  pinMode(P, OUTPUT);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(SW, INPUT);
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(motorOnOff, INPUT);
  MS = false;

}

// Test code

// Working Code

/*Servo Function */
void servoFunc(int deg) {
  int servomap = map(deg, 0, 1024, 775, 2225);
  
  if (servomap>1750){
    servo.write(servomap);
  }
  else if (servomap<1250){
    servo.write(servomap); 
  }
  else{
   servo.write(1500); 
  }  
}

/*Pump Function*/
void pumpFunc(bool onOff) {
  if (onOff == true) {
    digitalWrite(P, true);
  }
  else {
    digitalWrite(P, false);
  }  
}

void pumpFuncMain (int pwr) {
  if (pwr > 700) {
    pumpFunc(true);
    valveFunc(false);
  }
  else if (pwr < 700 && pwr > 400) {
    pumpFunc(false);
    valveFunc(false);
  }
  else {
    pumpFunc(false);
    valveFunc(true);
  }
}

/*Valve function*/
void valveFunc(bool openClose) {
  digitalWrite(V, openClose);  
}

/* Motor Function */
void forwFunc(int impPwr) {
  int pwr = map(impPwr, 0, 100, 0, 76.5);
  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  analogWrite(enA, pwr);
}

void backFunc(int impPwr) {
  int pwr = map(impPwr, 0, 100, 0, 76.5);
  int pwrN = 76.5 - pwr;
  Serial.println(pwrN);
  digitalWrite(in1, 0);
  digitalWrite(in2, 1);
  analogWrite(enA, pwrN);  
}

void stopFunc() {
  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  analogWrite(enA, 0);
}

void motorFunc(int inpPwr) {
  int pwr = map(inpPwr, 0, 1024, 0, 100);

  if (inpPwr > 700) {
    forwFunc(pwr);    
  }
  else if (inpPwr < 400) {
    backFunc(pwr);    
  }
  else {
    stopFunc();
  }  
}


void loop() {
  // put your main code here, to run repeatedly:

  /*
  servoFunc(90);
  pumpFunc(true);
  valveFunc(true);
  delay(1000);

  servoFunc(180);
  delay(1000);

  servoFunc(0);
  pumpFunc(false);
  valveFunc(false);u
  servoFunc(90);
  delay(2000);
  */

  //Serial.println(analogRead(VRx));
  delay(200);

  motorFunc(analogRead(VRx));
  //Serial.println(analogRead(VRx));
  

}
