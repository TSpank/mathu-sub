#include <RH_ASK.h>
#include <SPI.h>
#include <ServoTimer2.h>

//pin assignment
const int V = 2;
const int MB = 3;
const int MF = 5;
const int S = 6;
const int P = 9;


//Making servo channel
ServoTimer2 servo;

RH_ASK driver;

/////////////////////////create the data object
struct Signal{
  short throttle;
  bool dir;
  short rudder;
  bool pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

//////////////////////////Set all the data to base values
void ResetData(){
  data.throttle = 0;
  data.dir = true;
  data.rudder = 127;
  data.pump = false;
  data.valve = false;
}

void setup() {
  Serial.begin(9600);
  servo.attach(S);
  pinMode(MB,OUTPUT);
  pinMode(MF,OUTPUT);
  pinMode(V, OUTPUT);
  pinMode(P, OUTPUT);

  ResetData();
  if(!driver.init()){
    Serial.println("Initialisation Failed");
  }
}

//////////////////////////////motor control functions
void motorStop(){
  analogWrite(MB, 0);
  analogWrite(MF, 0);
}

void motorMove(short throttle, bool dir){
  if(dir == true){
    analogWrite(MB, 0);
    analogWrite(MF, throttle);
  }
  else if(dir == false){
    analogWrite(MB, throttle);
    analogWrite(MF, 0);
  }
}

void turn(int pwm){
  int servomap = map(pwm, 0, 255, 775, 2225);
  if (servomap>1700){
    servo.write(servomap);
  }
  else if (servomap<1300){
    servo.write(servomap); 
  }
  else{
   servo.write(1500); 
  }
}

void pump(bool val_p, bool val_v){
  digitalWrite(V, val_v);
  digitalWrite(P, val_p);
}

//////////////////////loop function to recieve and handle data
void loop() {
  Serial.println(data.rudder);
  // put your main code here, to run repeatedly:
  uint8_t buf[sizeof(data)];  
  uint8_t buflen = sizeof(data);

  if (driver.recv(buf, &buflen)) {
    memcpy(&data, &buf, buflen);

    //control the throttle
    if (data.throttle >5){
      motorMove(data.throttle, data.dir);
      }
    else{
      motorStop();
    }
    Serial.println(data.valve);
    Serial.println(data.pump);
    Serial.println(data.rudder);
    Serial.println("\n-------------------------------------------------\n");
    
    //control the servo
    turn(data.rudder);

    //control pump and valve
    pump(data.pump, data.valve);
  }
}
