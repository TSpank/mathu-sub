#include <RH_ASK.h>
#include <SPI.h>
#include <ServoTimer2.h>

#define FORW true
#define BACK false

//pin assignment
const int V = 2;
const int M_PWM = 5;
const int M_P = 4;
const int M_N = 3;
const int S = 6;
const int P_CTL = 8;
const int P_P = 9;
const int P_N =10;


//Making servo channel
ServoTimer2 servo;

RH_ASK driver;
//Motor Speed control variable
int MS;
//Step variable to control motor acceleration
int st = 4;

//create the data object
struct Signal{
  short throttle;
  bool dir;
  short rudder;
  bool pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

//Set all the data to base values
void ResetData(){
  data.throttle = 0;
  data.dir = FORW;
  data.rudder = 127;
  data.pump = false;
  data.valve = false;
  MS = 0;
}

void setup() {
  servo.attach(S);
  pinMode(V, OUTPUT);
  pinMode(M_PWM,OUTPUT);
  pinMode(M_P, OUTPUT);
  pinMode(M_N, OUTPUT);
  pinMode(P_P,OUTPUT);
  pinMode(P_N, OUTPUT);
  pinMode(P_CTL, OUTPUT);
  
  ResetData();
  driver.init();
}

//motor control function
void motor(short throttle, bool dir){
  if(throttle > 5){
    if(abs(MS) < throttle){      
      if(dir == FORW){
        MS+=st;
        if(MS >= 0){
          digitalWrite(M_P, HIGH);
          digitalWrite(M_N, LOW);
          analogWrite(M_PWM, MS);
        }
        else if(MS < 0){
          digitalWrite(M_P, LOW);
          digitalWrite(M_N, HIGH);
          analogWrite(M_PWM, -MS);
        }
      }
      else if(dir == BACK){
        MS-=st;
        if(MS >= 0){
          digitalWrite(M_P, HIGH);
          digitalWrite(M_N, LOW);
          analogWrite(M_PWM, MS);
        }
        if(MS < 0){
          digitalWrite(M_P, LOW);
          digitalWrite(M_N, HIGH);
          analogWrite(M_PWM, -MS);
        }
      }
    }
    else if(MS == throttle){
        digitalWrite(M_P, HIGH);
        digitalWrite(M_N, LOW);
        analogWrite(M_PWM, MS);
    }
    else if(MS == -throttle){
        digitalWrite(M_P, LOW);
        digitalWrite(M_N, HIGH);
        analogWrite(M_PWM, -MS);
    }
  }
  else{
    if(MS > 0){
      MS-=st;
      digitalWrite(M_P, HIGH);
      digitalWrite(M_N, LOW);
      analogWrite(M_PWM, MS);
    }
    else if(MS < 0){
      MS+=st;
      digitalWrite(M_P, LOW);
      digitalWrite(M_N, HIGH);
      analogWrite(M_PWM, -MS);
    }
    else{
      digitalWrite(M_P, LOW);
      digitalWrite(M_N, LOW);
      analogWrite(M_PWM, 0);
    }
  }
}
//rudder/servo control function
void turn(int pwm){
  int servomap = map(pwm, 0, 255, 775, 2225);
  if (servomap>1700){
    servo.write(servomap);
  }
  else if (servomap<1300){
    servo.write(servomap); 
  }
  else{
   servo.write(1500); 
  }
}
//pump and valve control function
void pump(bool val_p, bool val_v){
  digitalWrite(V, val_v);

  if(val_p){
    digitalWrite(P_CTL, HIGH);
    digitalWrite(P_P, HIGH);
    digitalWrite(P_N, LOW);
  }
  else{
    digitalWrite(P_CTL, LOW);
    digitalWrite(P_P, LOW);
    digitalWrite(P_N, LOW);
  }
}

//loop function to recieve and handle data
void loop() {
  // put your main code here, to run repeatedly:
  uint8_t buf[sizeof(data)];  
  uint8_t buflen = sizeof(data);

  if (driver.recv(buf, &buflen)) {
    memcpy(&data, &buf, buflen);

    //control the throttle
    motor(data.throttle, data.dir);
    
    //control the servo
    turn(data.rudder);

    //control pump and valve
    pump(data.pump, data.valve);
  }
}
