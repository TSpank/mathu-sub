#include <RH_ASK.h>
#include <SPI.h>
#include <ServoTimer2.h>

#define FORW true
#define BACK false

//pin assignment
const int V = 2;
const int MB = 3;
const int MF = 5;
const int S = 6;
const int P_GND = 8;
const int P = 9;


//Making servo channel
ServoTimer2 servo;

RH_ASK driver;
//Motor Speed control variable
int MS;

//create the data object
struct Signal{
  short throttle;
  bool dir;
  short rudder;
  bool pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

//Set all the data to base values
void ResetData(){
  data.throttle = 0;
  data.dir = FORW;
  data.rudder = 127;
  data.pump = false;
  data.valve = false;
  MS = 0;
}

void setup() {
  Serial.begin(9600);
  servo.attach(S);
  pinMode(MB,OUTPUT);
  pinMode(MF,OUTPUT);
  pinMode(V, OUTPUT);
  pinMode(P, OUTPUT);
  pinMode(P_GND, OUTPUT);

  ResetData();
  if(!driver.init()){
    Serial.println("Initialisation Failed");
  }
}

//motor control function
void motor(short throttle, bool dir){
  if(throttle > 5){
    if(abs(MS) < throttle){      
      if(dir == FORW){
        MS++;
        if(MS > 0){
          analogWrite(MF, abs(MS));
          analogWrite(MB, 0);
        }
        else if(MS < 0){
          analogWrite(MF, 0);
          analogWrite(MB, abs(MS));
        }
        delay(7);
      }
      else if(dir == BACK){
        MS--;
        if(MS > 0){
          analogWrite(MF, abs(MS));
          analogWrite(MB, 0);
        }
        else if(MS < 0){
          analogWrite(MF, 0);
          analogWrite(MB, abs(MS));
        }
        delay(7);
      }
    }
    else if(MS == throttle){
        analogWrite(MF, abs(MS));
        analogWrite(MB, 0);
    }
    else if(MS == -throttle){
        analogWrite(MF, 0);
        analogWrite(MB, abs(MS));
    }
  }
  else{
    if(MS > 0){
      MS--;
      analogWrite(MF, abs(MS));
      analogWrite(MB, 0);
      delay(7);
    }
    else if(MS < 0){
      MS++;
      analogWrite(MF, 0);
      analogWrite(MB, abs(MS));
      delay(7);
    }
    else{
      analogWrite(MF, 0);
      analogWrite(MB, 0);
    }
  }
}
//rudder/servo control function
void turn(int pwm){
  int servomap = map(pwm, 0, 255, 775, 2225);
  if (servomap>1700){
    servo.write(servomap);
  }
  else if (servomap<1300){
    servo.write(servomap); 
  }
  else{
   servo.write(1500); 
  }
}
//pump control function
void pump(bool val_p, bool val_v){
  digitalWrite(V, val_v);
  digitalWrite(P, val_p);
  digitalWrite(P_GND, 0);
}

//loop function to recieve and handle data
void loop() {
  // put your main code here, to run repeatedly:
  uint8_t buf[sizeof(data)];  
  uint8_t buflen = sizeof(data);

  if (driver.recv(buf, &buflen)) {
    memcpy(&data, &buf, buflen);

    //control the throttle
    motor(data.throttle, data.dir);
    
    //control the servo
    turn(data.rudder);

    //control pump and valve
    pump(data.pump, data.valve);



    Serial.print(data.throttle);
    if(data.dir)
      Serial.println("  forward\n");
    else if(!data.dir)
      Serial.println("  backward\n");
  }
}
