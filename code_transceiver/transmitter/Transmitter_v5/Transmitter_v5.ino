#include <RH_ASK.h>
#include <SPI.h>

const uint8_t MX = A0;
const uint8_t MY = A1;
const uint8_t PY = A2;

RH_ASK driver;

struct Signal
{
  short throttle;
  bool dir;
  short rudder;
  bool pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

void mapping(int motorX, int motorY, int pumpY){
  if(motorY > 560){
    data.throttle = map(motorY, 560, 1023, 0, 255);
    data.dir = true;
  }
  else if(motorY < 464){
    data.throttle = map(motorY, 0, 464, 255, 0);
    data.dir = false;
  }
  else {
    data.throttle = 0;
    data.dir = true;
  }
  data.rudder = map(motorX, 0, 1023, 0, 255);

  if(pumpY > 620){
    data.valve = false;
    data.pump = true;
  }
  else if(pumpY < 404){
    data.valve = true;
    data.pump = false;
  }
  else{
    data.valve = false;
    data.pump = false;
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  if(!driver.init()){
    Serial.println("Initialisation Failed");
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  mapping(analogRead(MX), analogRead(MY), analogRead(PY));
  driver.send((uint8_t *)&data, sizeof(data));
  driver.waitPacketSent();
  
}
