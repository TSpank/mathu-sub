#include <RH_ASK.h>
#include <SPI.h>

const uint8_t MX = A2;
const uint8_t MY = A1;
const uint8_t PY = A0;

RH_ASK driver;

struct Signal {
  short throttle;
  short rudder;
  short pump;
};

typedef struct Signal Data;
Data data;

void getInfo(int joyLx, int joyLy, int joyRy) {
  data.throttle = 1023 - joyLy;
  data.rudder = 1023 - joyLx;
  data.pump = 1023 - joyRy;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  if(!driver.init()){
    Serial.println("Initialisation Failed");
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  getInfo(analogRead(MX), analogRead(MY), analogRead(PY));
  driver.send((uint8_t *)&data, sizeof(data));
  driver.waitPacketSent();
  Serial.print("Throttle: ");
  Serial.print(data.throttle);
  Serial.print("  Rudder: ");
  Serial.print(data.rudder);
  Serial.print("  Pump: ");
  Serial.println(data.pump);

}
