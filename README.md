# MathU Sub

## Code Type
- Arduino (C++)
- CAD files
- .gcode

## Authors and acknowledgment
- MathU S.T.E.M. Sub-team.
    - Thomas
    - Liam
    - Josh


## Project status
In progress.

## Schematics
- Receiver
  -  [offline](Diagrams/receiverSchematic_v1.pdf)
  -  [online](https://gitlab.com/TSpank/mathu-sub/-/blob/main/Diagrams/receiverSchematic_v1.pdf)

- Transmitter
  - [offline](Diagrams/transmitterSchematic_v1.pdf)
  - [online](https://gitlab.com/TSpank/mathu-sub/-/blob/main/Diagrams/transmitterSchematic_v1.pdf)

## General Information
### Transmitter and Receiver Modules
- 433 MHz band chosen due to simplicity and cost efficiency
- Created antennas for both modules
    - Transmitter module has straight (17.2 cm = 1/4 of the wavelength)
        - Needs to be straight to enhance the signal
    - Receiver module has store bought coiled antenna for 433 MHz
        - the coils should not touch
- Voltage:
    - 12V for transmitter for maximum range
    - 5V for receiver unit due to specifications.
- Range:
    - approximately 20 metres with max voltage and not interferance
    - interferances noted:
        - conducting material in the line of site of the pair (big interferance)
        - non-conducting material in the line of site of the pair (smaller interferance)
- Links:
  - [Transmitter: Botshop](https://www.botshop.co.za/product/rf-433mhz-transmitter-module-zf-3/)
  - [Receiver: Botshop](https://www.botshop.co.za/product/rf-433mhz-receiver-module-zr4/)
  - [Antennas: MicroRobotics](https://www.robotics.org.za/AF4394?search=433%20MHz%20antenna)
  - [Tutorial from DroneBot Workshop](https://dronebotworkshop.com/433mhz-rf-modules-arduino/)

### H-bridge
- H-bridge code: L298N
- Input
  - 12V: Straight from $V_{in}$
  - GND
  - enA: PWM
  - IN1: Data
  - IN2: Data
- Output
  - Out2: Motor_input1
  - Out2: Motor_input2
- Ampere Rating: 2A
- Link: [Botshop Link](https://www.botshop.co.za/product/dual-l298n-h-bridge-stepper-motor-driver-2a/)
- [Data sheet](https://www.sparkfun.com/datasheets/Robotics/L298_H_Bridge.pdf)

### DC High Torque Motor
- $V_{in}$: 0-12V
- can switch direction by switching terminal connections
- Varying voltage will vary speed.
- Connect straight to the [H-bridge](#h-bridge)
- High torque was chosen to overcome the friction of the glass/plastic interface with the magnets.
- Link: [Botshop Link](https://www.botshop.co.za/product/micro-metal-gear-motor-501-12v-500rpm/)

### Mosfets
- Mosfet Code: FQP30N06L
- MOSFET stands for **M**etal-**O**xide-**S**emiconductor **F**ield-**E**ffect **T**ransistor. 
  - It is a field-effect transistor with a MOS structure. Typically, the MOSFET is a three-terminal device with gate (G), drain (D) and source (S) terminals.
- [Data sheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/52370/FAIRCHILD/FQP30N06L.html)
- Used to switch on Pump or Valve by sending a digital signal to the gate.
  - [Check schematic](https://gitlab.com/TSpank/mathu-sub/-/blob/main/Diagrams/MOSFET_schematic.pdf)



### DC Pump
- Power: 17W
- Voltage: 12V
- GND: connected to the mosfet
- Ampere: 1.4A
  - due to the huge current, 3 strand wire was used to carry the current.
  - Link to find AWG to current rating is here: [engineeringtoolbox](https://www.engineeringtoolbox.com/wire-gauges-d_419.html)
- Link: [Botshop Link](https://www.botshop.co.za/product/2l-min-air-or-vacuum-pump-dc12v/)
- Connected with a Zener Diode in parallel to reduce the Back EMF (Electric Motive Force) when dc Motor of the pump is switched off
  - Code: 1N4001
  - [Datasheet](https://www.rectron.com/data_sheets/1n4001-1n4007.pdf)

### Air Valve Soleniod
- $V_{in}$: 6V (very exact)
- GND: Connected to the mosfet
- Link: [MicroRobotics](https://www.robotics.org.za/DFR0866?search=air%20valve)
- Connected with a Zener Diode in parallel to reduce the Back EMF (Electric Motive Force) when Solenoid of the valve is switched off
  - Code: 1N4001
  - [Datasheet](https://www.rectron.com/data_sheets/1n4001-1n4007.pdf)

### Screw Input Data
| Screw Label | Info | Module |
| ---: | :--- | --- |
| 1 | GND | Receiver|
| 2 | DATA | Receiver |
| 3 | +5V | Receiver |
| a | GND | Servo |
| b | +6V | Servo |
| c | DATA | Servo |
| d | +6V | Valve |
| e | GND | Valve |
| f | GND | Pump |
| g | +12V | Pump |
| h | Out1 | Motor |
| i | Out2 | Motor |
