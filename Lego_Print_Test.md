# LEGO PRINT TEST
#


| Part Number | Quantity | Schematic? | Print Test 1 | Print Test 2 | Print Test 3 |
| ----------- | -------- |------------| -------------|--------------|--------------|
| 39794 | #1 |Found | -| -| -|
| 32140 | #7 |Found | Fail | Success| -|
| 14720 | #2 |Not Found | -| -| -|
| 32524 | #8 |Found | -| -| -|
| 32316 | #5 |Found | Success | -| -|
| 2780 | #60 |Found | -| -| -|
| 71710 | #3 |Not Found | -| -| -|
| 41239 | #1 |Found | -| -| -|
| 15100 | #2 |Found | -| -| -|
| 32526 | #3 |Found | -| -| -|
| 43093 | #4 |Found | -| -| -|
| 11214 | #1 |Found | -| -| -|
| 64179 | #3 |Found | -| -| -|
| 40490 | #3 |Found | -| -| -|
| 6632 | #2 |Found | Fail| -| -|
| 3713 | #3 |Found | -| -| -|
| 6558 | #7 |Found | -| -| -|
| 23948 | #1 |Found | Success| -| -|
| 3648 | #3 |Found | -| -| -|
| 3647 | #3 | Not Found | -| -| -|
| 24316 | #1 |Found | -| -| -|
| 32062| #2 |Found | Success | -| -|
| 6538c/59443 | #1 |Found | -| -| -|
| 32209 | #1 |Found | -| -| -|
| 4552348 | #2 |Not Found | -| -| -|
| 6330986 | #1 |Not Found | -| -| -|
| 6335279 | #1 |Not Found | -| -| -|

### Print Test 1.0: 32316, 32140, 23948
- 32316 did not adhere properly to bed - print fail
- 32140 and 23948 were part of the attempted print,
 and had to be scrapped alongside 32316. 
- 32140 and 23948 will not be regarded as having failed. 

### Print Test 1.1 32316, 32140, 23948
- print success!
- 32316 - Workable, disassembly is difficult
- 32140 - Round holes workable, axle holes can't accomodate axles!
- 23948 - Fits axle holes, however is prone to sliding around.

### Print Run 1.0
- 32140 #7
- 14720 #2
- 39794 #1




